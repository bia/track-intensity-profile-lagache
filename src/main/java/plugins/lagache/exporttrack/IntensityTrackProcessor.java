package plugins.lagache.exporttrack;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;

import icy.file.xls.XlsManager;
import icy.gui.dialog.MessageDialog;
import icy.gui.frame.progress.AnnounceFrame;
import icy.gui.util.GuiUtil;
import icy.sequence.Sequence;
import icy.type.collection.array.ArrayUtil;
import plugins.fab.trackmanager.PluginTrackManagerProcessor;
import plugins.fab.trackmanager.TrackGroup;
import plugins.fab.trackmanager.TrackSegment;

/**
 * A TrackProcessor to monitor detection fluorescence along time
 * 
 * @author nicolas chenouard
 * date 11/03/2012
 */

public class IntensityTrackProcessor extends PluginTrackManagerProcessor implements ActionListener
{
    // TODO: disable analysis thread when processor is disabled

    /*
     * ArrayList<JFreeChart> charts;
     * JPanel chartsPanel = new JPanel();
     * JCheckBox displayLegendBox = new JCheckBox("Display legend");
     */

    JButton setTrackSetA = new JButton("");
    JButton exportToXLSButton = new JButton("export results to excel");

    ArrayList<TrackSegment> trackSegmentSetA = new ArrayList<TrackSegment>();

    JPanel timeClipPanel;
    JPanel averagingDiskPanel;
    JPanel averagingDiskNoBackgroundPanel;
    JPanel averagingMaskPanel;

    // JComboBox<AveragingType> averagingTypeBox;
    JComboBox averagingTypeBox;
    CardLayout methodCardLayout;
    JPanel averagingMethodPanel;

    String[] criteria = new String[] {"Mean intensity", "Max intensity", "Min intensity", "Median intensity",
            "Intensity sum", "Intensity variance"};

    /*
     * JComboBox criteriaBox = new JComboBox(criteria);
     * JComboBox criteriaBox2 = new JComboBox(criteria);
     * JComboBox criteriaBox3 = new JComboBox(criteria);
     */

    SpinnerNumberModel diskDiameterModel = new SpinnerNumberModel(2, 0, 1000, 0.5);
    SpinnerNumberModel diskOutterDiameterModel = new SpinnerNumberModel(4, 2, 1000, 0.5);
    SpinnerNumberModel timeClipModel = new SpinnerNumberModel(2, 0, 1000, 1);

    ArrayList<TrackAnalysis> analyzedTracks = new ArrayList<TrackAnalysis>();

    Sequence selectedSequence = null;

    public IntensityTrackProcessor()
    {
        this.setName("Intensity profile");

        this.panel.setLayout(new BorderLayout());
        JPanel northnorthPane = new JPanel();
        northnorthPane.setLayout(new BorderLayout());
        northnorthPane.add(GuiUtil.createLineBoxPanel(setTrackSetA));
        setTrackSetA.addActionListener(this);

        this.panel.add(northnorthPane, BorderLayout.PAGE_START);

        JPanel centerPanel = new JPanel();
        centerPanel.setLayout(new BorderLayout());

        JPanel averagingTypePanel = new JPanel(new GridLayout(1, 1));
        averagingTypePanel.add(new JLabel("Averaging type"));
        averagingTypeBox = new JComboBox(AveragingType.values());

        averagingTypeBox.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                methodCardLayout.show(averagingMethodPanel, averagingTypeBox.getSelectedItem().toString());
            }
        });
        averagingTypePanel.add(averagingTypeBox);
        centerPanel.add(averagingTypePanel, BorderLayout.CENTER);

        methodCardLayout = new CardLayout();
        averagingMethodPanel = new JPanel(methodCardLayout);
        centerPanel.add(averagingMethodPanel, BorderLayout.SOUTH);

        averagingDiskPanel = new JPanel(new GridLayout(1, 1));
        averagingDiskPanel.add(new JLabel("Diameter of the disk area in pixels"));
        JSpinner diskDiameterSpinner = new JSpinner(diskDiameterModel);
        /*
         * diskDiameterSpinner.addChangeListener(new ChangeListener(){
         * 
         * @Override
         * public void stateChanged(ChangeEvent e) {
         * AveragingType averagingMethod = (AveragingType) averagingTypeBox.getSelectedItem();
         * }});
         */
        averagingDiskPanel.add(diskDiameterSpinner);
        averagingMethodPanel.add(averagingDiskPanel, AveragingType.DISK.toString());

        averagingDiskNoBackgroundPanel = new JPanel(new GridLayout(2, 1));
        averagingDiskNoBackgroundPanel.add(new JLabel("Diameter of the disk area in pixels"));
        JSpinner diskDiameterSpinner2 = new JSpinner(diskDiameterModel);
        /*
         * diskDiameterSpinner2.addChangeListener(new ChangeListener(){
         * 
         * @Override
         * public void stateChanged(ChangeEvent e) {
         * AveragingType averagingMethod = (AveragingType) averagingTypeBox.getSelectedItem();
         * }});
         */
        averagingDiskNoBackgroundPanel.add(diskDiameterSpinner2);
        JSpinner diskOutterDiameterSpinner2 = new JSpinner(diskOutterDiameterModel);
        /*
         * diskOutterDiameterSpinner2.addChangeListener(new ChangeListener(){
         * 
         * @Override
         * public void stateChanged(ChangeEvent e) {
         * AveragingType averagingMethod = (AveragingType) averagingTypeBox.getSelectedItem();
         * }});
         */
        averagingDiskNoBackgroundPanel.add(new JLabel("Diameter of the background disk area in pixels"));
        averagingDiskNoBackgroundPanel.add(diskOutterDiameterSpinner2);
        /*
         * averagingDiskNoBackgroundPanel.add(new JLabel("Criterion to display"));
         * averagingDiskNoBackgroundPanel.add(criteriaBox2);
         */

        averagingMethodPanel.add(averagingDiskNoBackgroundPanel, AveragingType.DISK_BACKGROUND_CORRECTED.toString());

        averagingMaskPanel = new JPanel(new GridLayout(1, 1));
        averagingMaskPanel.add(new JLabel("Missing detections masks replaced by value at center of mass."));
        averagingMaskPanel.add(new JLabel(""));
        /*
         * averagingMaskPanel.add(new JLabel("Criterion to display"));
         * averagingMaskPanel.add(criteriaBox3);
         */

        averagingMethodPanel.add(averagingMaskPanel, AveragingType.SPOT_MASK.toString());

        this.panel.add(centerPanel, BorderLayout.CENTER);

        JPanel southPane = new JPanel();
        southPane.setLayout(new BorderLayout());

        southPane.add(GuiUtil.createLineBoxPanel(exportToXLSButton));
        exportToXLSButton.addActionListener(this);
        this.panel.add(southPane, BorderLayout.SOUTH);

        JPanel westPane = new JPanel();
        westPane.setLayout(new BorderLayout());
        timeClipPanel = new JPanel(new GridLayout(1, 1));
        timeClipPanel.add(new JLabel("Time lag to extend tracks"));
        JSpinner timeClipSpinner = new JSpinner(timeClipModel);
        timeClipPanel.add(timeClipSpinner);
        westPane.add(timeClipPanel);
        this.panel.add(westPane, BorderLayout.WEST);

        refreshDisplay();

    }

    @Override
    public void actionPerformed(ActionEvent e)
    {

        if (e.getSource() == setTrackSetA)
        {
            trackSegmentSetA.clear();
            for (TrackGroup group : trackPool.getTrackGroupList())
            {
                for (TrackSegment trackSegment : group.getTrackSegmentList())
                {
                    if (trackSegment.isAllDetectionSelected())
                    {
                        trackSegmentSetA.add(trackSegment);
                    }
                }
            }
        }

        if (e.getSource() == exportToXLSButton)
        {
            if (!isEnabled())
                return;
            Sequence sequence = this.trackPool.getDisplaySequence();

            if (sequence != selectedSequence)
            {
                changeSelectedSequence(sequence);
                return;
            }

            double diskRadius = ((Number) (diskDiameterModel.getValue())).doubleValue();
            double diskOutterRadius = ((Number) (diskOutterDiameterModel.getValue())).doubleValue();
            int timeClip = ((Number) (timeClipModel.getValue())).intValue();
            AveragingType averagingMethod = (AveragingType) averagingTypeBox.getSelectedItem();
            fillSeries(trackSegmentSetA, selectedSequence, diskRadius, diskOutterRadius, timeClip, averagingMethod);// TODO:
                                                                                                                    // should
                                                                                                                    // be
                                                                                                                    // outside
                                                                                                                    // of
                                                                                                                    // the
                                                                                                                    // lock
            exportToXLS(analyzedTracks, (AveragingType) averagingTypeBox.getSelectedItem(), timeClip, sequence);
        }

        refreshDisplay();

    }

    private void exportToXLS(ArrayList<TrackAnalysis> trackAnalysisList, final AveragingType averType, int timeClip,
            Sequence sequence)
    {
        JFileChooser chooser = new JFileChooser();
        chooser.setDialogTitle("Choose xls file.");
        // chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        int returnVal = chooser.showOpenDialog(this.panel);
        if (returnVal != JFileChooser.APPROVE_OPTION)
            return;

        File file = chooser.getSelectedFile();
        if (file == null)
            return;
        if (!file.getName().endsWith(".xls"))
        {
            try
            {
                file = new File(file.getCanonicalPath() + ".xls");
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
        }
        final ArrayList<TrackAnalysis> tList = new ArrayList<TrackAnalysis>();
        for (TrackAnalysis ta : trackAnalysisList)
            if (ta.averagingType == averType)
                tList.add(ta);
        if (tList.size() < 1)
            return;
        final File XLSFile = file;

        // AnnounceFrame announce1 = new AnnounceFrame("Saving results");
        // save disk averaging results first

        // check that the number of tracks to save does not exceed the number of allowed columns in the Excel file: 255
        // otherwise, create multiple files
        ArrayList<ArrayList<TrackAnalysis>> tsListList = new ArrayList<ArrayList<TrackAnalysis>>();
        int maxNumCol = 255;
        int numFiles = (int) Math.ceil(tList.size() / (double) maxNumCol);
        if (numFiles > 1)
        {
            for (int k = 0; k < numFiles; k++)
            {
                ArrayList<TrackAnalysis> trkList = new ArrayList<TrackAnalysis>();

                for (int i = 0; i < maxNumCol; i++)
                {
                    if ((i + (k * maxNumCol)) < tList.size())
                        trkList.add(tList.get(i + (k * maxNumCol)));
                }

                tsListList.add(trkList);
            }
        }
        else
        {
            tsListList.add(tList);
        }

        for (int k = 0; k < numFiles; k++)
        {
            if (k >= tsListList.size())
            {
                System.out.println("Error: could not get any tracks for XLS file #" + k);
                break;
            }

            ArrayList<TrackAnalysis> trackList = tsListList.get(k);

            XlsManager xls = null;
            try
            {
                if (numFiles < 2)
                    xls = new XlsManager(XLSFile);
                else
                {
                    String filePath = XLSFile.getCanonicalPath();
                    File XLSFile2 = new File(
                            filePath.substring(0, filePath.toCharArray().length - 4) + "_" + k + ".xls");
                    xls = new XlsManager(XLSFile2);
                }
            }
            catch (IOException e)
            {

                MessageDialog.showDialog("Cannot open file.", MessageDialog.ERROR_MESSAGE);
                return;
            }

            int numChannels = computeMaxNumChannels(trackList);

            switch (averType)
            {
                case DISK:
                    for (int c = 0; c < numChannels; c++)
                    {
                        for (int i = 0; i < 6; i++)
                        {
                            xls.createNewPage("Channel " + c + " - " + criteria[i]);
                            // create the time axis
                            int maxT = 0;
                            int minT = Integer.MAX_VALUE;
                            for (TrackAnalysis trkAnalysis : trackList)
                            {
                                if (!trkAnalysis.track.getDetectionList().isEmpty())
                                {
                                    if (trkAnalysis.track.getLastDetection().getT() > maxT)
                                        maxT = trkAnalysis.track.getLastDetection().getT();
                                    if (trkAnalysis.track.getFirstDetection().getT() < minT)
                                        minT = trkAnalysis.track.getFirstDetection().getT();
                                }
                            }
                            xls.setLabel(0, 0, "Frame number");
                            int mini = Math.max(0, minT - timeClip);
                            int maxi = Math.min(sequence.getSizeT() - 1, maxT + timeClip);
                            for (int t = mini; t <= maxi; t++)
                                xls.setNumber(0, t + 1, t);
                            int cntTrack = 0;
                            for (TrackAnalysis trka : trackList)
                            {
                                TrackAnalysisDisk trkAnalysis = (TrackAnalysisDisk) trka;
                                if (!trkAnalysis.track.getDetectionList().isEmpty())
                                {
                                    cntTrack++;
                                    xls.setLabel(cntTrack, 0, trkAnalysis.description);
                                    int firstT = Math.max(0, trkAnalysis.track.getFirstDetection().getT() - timeClip);
                                    switch (i)
                                    {
                                        case 0:
                                        {
                                            if (trkAnalysis.meanIntensity.length >= c)
                                                for (int j = 0; j < trkAnalysis.meanIntensity[c].getItemCount(); j++)
                                                    xls.setNumber(cntTrack, firstT + j + 1,
                                                            trkAnalysis.meanIntensity[c].getY(j).doubleValue());
                                            break;
                                        }
                                        case 1:
                                            if (trkAnalysis.maxIntensity.length >= c)
                                                for (int j = 0; j < trkAnalysis.maxIntensity[c].getItemCount(); j++)
                                                    xls.setNumber(cntTrack, firstT + j + 1,
                                                            trkAnalysis.maxIntensity[c].getY(j).doubleValue());
                                            break;
                                        case 2:
                                            if (trkAnalysis.minIntensity.length >= c)
                                                for (int j = 0; j < trkAnalysis.minIntensity[c].getItemCount(); j++)
                                                    xls.setNumber(cntTrack, firstT + j + 1,
                                                            trkAnalysis.minIntensity[c].getY(j).doubleValue());
                                            break;
                                        case 3:
                                            if (trkAnalysis.medianIntensity.length >= c)
                                                for (int j = 0; j < trkAnalysis.medianIntensity[c].getItemCount(); j++)
                                                    xls.setNumber(cntTrack, firstT + j + 1,
                                                            trkAnalysis.medianIntensity[c].getY(j).doubleValue());
                                            break;
                                        case 4:
                                            if (trkAnalysis.sumIntensity.length >= c)
                                                for (int j = 0; j < trkAnalysis.sumIntensity[c].getItemCount(); j++)
                                                    xls.setNumber(cntTrack, firstT + j + 1,
                                                            trkAnalysis.sumIntensity[c].getY(j).doubleValue());
                                            break;
                                        case 5:
                                            if (trkAnalysis.varIntensity.length >= c)
                                                for (int j = 0; j < trkAnalysis.varIntensity[c].getItemCount(); j++)
                                                    xls.setNumber(cntTrack, firstT + j + 1,
                                                            trkAnalysis.varIntensity[c].getY(j).doubleValue());
                                            break;
                                    }
                                }
                            }
                        }
                    }
                    break;
                case SPOT_MASK:
                    for (int c = 0; c < numChannels; c++)
                    {
                        for (int i = 0; i < 6; i++)
                        {
                            xls.createNewPage("Channel " + c + " - " + criteria[i]);
                            // create the time axis
                            int maxT = 0;
                            int minT = Integer.MAX_VALUE;
                            for (TrackAnalysis trkAnalysis : trackList)
                            {
                                if (!trkAnalysis.track.getDetectionList().isEmpty())
                                {
                                    if (trkAnalysis.track.getLastDetection().getT() > maxT)
                                        maxT = trkAnalysis.track.getLastDetection().getT();
                                    if (trkAnalysis.track.getFirstDetection().getT() < minT)
                                        minT = trkAnalysis.track.getFirstDetection().getT();
                                }
                            }
                            xls.setLabel(0, 0, "Frame number");
                            int mini = Math.max(0, minT - timeClip);
                            int maxi = Math.min(sequence.getSizeT() - 1, maxT + timeClip);
                            for (int t = mini; t <= maxi; t++)
                                xls.setNumber(0, t + 1, t);
                            int cntTrack = 0;
                            for (TrackAnalysis trka : trackList)
                            {
                                TrackAnalysisMask trkAnalysis = (TrackAnalysisMask) trka;
                                if (!trkAnalysis.track.getDetectionList().isEmpty())
                                {
                                    cntTrack++;
                                    xls.setLabel(cntTrack, 0, trkAnalysis.description);
                                    int firstT = Math.max(0, trkAnalysis.track.getFirstDetection().getT() - timeClip);
                                    switch (i)
                                    {
                                        case 0:
                                        {
                                            if (trkAnalysis.meanIntensity.length >= c)
                                                for (int j = 0; j < trkAnalysis.meanIntensity[c].getItemCount(); j++)
                                                    xls.setNumber(cntTrack, firstT + j + 1,
                                                            trkAnalysis.meanIntensity[c].getY(j).doubleValue());
                                            break;
                                        }
                                        case 1:
                                            if (trkAnalysis.maxIntensity.length >= c)
                                                for (int j = 0; j < trkAnalysis.maxIntensity[c].getItemCount(); j++)
                                                    xls.setNumber(cntTrack, firstT + j + 1,
                                                            trkAnalysis.maxIntensity[c].getY(j).doubleValue());
                                            break;
                                        case 2:
                                            if (trkAnalysis.minIntensity.length >= c)
                                                for (int j = 0; j < trkAnalysis.minIntensity[c].getItemCount(); j++)
                                                    xls.setNumber(cntTrack, firstT + j + 1,
                                                            trkAnalysis.minIntensity[c].getY(j).doubleValue());
                                            break;
                                        case 3:
                                            if (trkAnalysis.medianIntensity.length >= c)
                                                for (int j = 0; j < trkAnalysis.medianIntensity[c].getItemCount(); j++)
                                                    xls.setNumber(cntTrack, firstT + j + 1,
                                                            trkAnalysis.medianIntensity[c].getY(j).doubleValue());
                                            break;
                                        case 4:
                                            if (trkAnalysis.sumIntensity.length >= c)
                                                for (int j = 0; j < trkAnalysis.sumIntensity[c].getItemCount(); j++)
                                                    xls.setNumber(cntTrack, firstT + j + 1,
                                                            trkAnalysis.sumIntensity[c].getY(j).doubleValue());
                                            break;
                                        case 5:
                                            if (trkAnalysis.varIntensity.length >= c)
                                                for (int j = 0; j < trkAnalysis.varIntensity[c].getItemCount(); j++)
                                                    xls.setNumber(cntTrack, firstT + j + 1,
                                                            trkAnalysis.varIntensity[c].getY(j).doubleValue());
                                            break;
                                    }
                                }
                            }
                        }
                    }
                    break;
                case DISK_BACKGROUND_CORRECTED:
                    for (int c = 0; c < numChannels; c++)
                    {
                        for (int i = 0; i < 6; i++)
                        {
                            xls.createNewPage("Channel " + c + " - " + criteria[i]);
                            // create the time axis
                            int maxT = 0;
                            int minT = Integer.MAX_VALUE;
                            for (TrackAnalysis trkAnalysis : trackList)
                            {
                                if (!trkAnalysis.track.getDetectionList().isEmpty())
                                {
                                    if (trkAnalysis.track.getLastDetection().getT() > maxT)
                                        maxT = trkAnalysis.track.getLastDetection().getT();
                                    if (trkAnalysis.track.getFirstDetection().getT() < minT)
                                        minT = trkAnalysis.track.getFirstDetection().getT();
                                }
                            }
                            xls.setLabel(0, 0, "Frame number");
                            int mini = Math.max(0, minT - timeClip);
                            int maxi = Math.min(sequence.getSizeT() - 1, maxT + timeClip);
                            for (int t = mini; t <= maxi; t++)
                                xls.setNumber(0, t + 1, t);
                            int cntTrack = 0;
                            for (TrackAnalysis trka : trackList)
                            {
                                TrackAnalysisDiskNoBackground trkAnalysis = (TrackAnalysisDiskNoBackground) trka;
                                if (!trkAnalysis.track.getDetectionList().isEmpty())
                                {
                                    cntTrack++;
                                    xls.setLabel(cntTrack, 0, trkAnalysis.description);
                                    int firstT = Math.max(0, trkAnalysis.track.getFirstDetection().getT() - timeClip);
                                    switch (i)
                                    {
                                        case 0:
                                        {
                                            if (trkAnalysis.meanIntensity.length >= c)
                                                for (int j = 0; j < trkAnalysis.meanIntensity[c].getItemCount(); j++)
                                                    xls.setNumber(cntTrack, firstT + j + 1,
                                                            trkAnalysis.meanIntensity[c].getY(j).doubleValue());
                                            break;
                                        }
                                        case 1:
                                            if (trkAnalysis.maxIntensity.length >= c)
                                                for (int j = 0; j < trkAnalysis.maxIntensity[c].getItemCount(); j++)
                                                    xls.setNumber(cntTrack, firstT + j + 1,
                                                            trkAnalysis.maxIntensity[c].getY(j).doubleValue());
                                            break;
                                        case 2:
                                            if (trkAnalysis.minIntensity.length >= c)
                                                for (int j = 0; j < trkAnalysis.minIntensity[c].getItemCount(); j++)
                                                    xls.setNumber(cntTrack, firstT + j + 1,
                                                            trkAnalysis.minIntensity[c].getY(j).doubleValue());
                                            break;
                                        case 3:
                                            if (trkAnalysis.medianIntensity.length >= c)
                                                for (int j = 0; j < trkAnalysis.medianIntensity[c].getItemCount(); j++)
                                                    xls.setNumber(cntTrack, firstT + j + 1,
                                                            trkAnalysis.medianIntensity[c].getY(j).doubleValue());
                                            break;
                                        case 4:
                                            if (trkAnalysis.sumIntensity.length >= c)
                                                for (int j = 0; j < trkAnalysis.sumIntensity[c].getItemCount(); j++)
                                                    xls.setNumber(cntTrack, firstT + j + 1,
                                                            trkAnalysis.sumIntensity[c].getY(j).doubleValue());
                                            break;
                                        case 5:
                                            if (trkAnalysis.varIntensity.length >= c)
                                                for (int j = 0; j < trkAnalysis.varIntensity[c].getItemCount(); j++)
                                                    xls.setNumber(cntTrack, firstT + j + 1,
                                                            trkAnalysis.varIntensity[c].getY(j).doubleValue());
                                            break;
                                    }
                                }
                            }
                        }
                    }
                    break;
                default:
                    break;
            }

            xls.SaveAndClose();
        }
        new AnnounceFrame("Results saved");
    }

    @Override
    public void Close()
    {
    }

    private void changeSelectedSequence(Sequence selectedSequence)
    {
        if (selectedSequence != this.selectedSequence)
        {
            // if (this.selectedSequence!=null)
            // this.selectedSequence.removePainter(this);
            this.selectedSequence = selectedSequence;
            // this.selectedSequence.addPainter(this);
        }
    }

    private void refreshDisplay()
    {

        boolean okToCompute = true;

        if (trackSegmentSetA.size() == 0)
        {
            okToCompute = false;
            setTrackSetA.setText("Put current selection in track set A");
        }
        else
        {
            setTrackSetA.setText("Track selected in set A : " + trackSegmentSetA.size());
        }

        exportToXLSButton.setEnabled(okToCompute);
        analyzedTracks.clear();

    }

    @Override
    public void displaySequenceChanged()
    {
        changeSelectedSequence(getFocusedSequence());
    }

    private int computeMaxNumChannels(ArrayList<TrackAnalysis> analyzedTrkList)
    {
        int maxNumChannels = 0;
        for (TrackAnalysis tr : analyzedTrkList)
            if (tr.sequence.getSizeC() > maxNumChannels)
                maxNumChannels = tr.sequence.getSizeC();
        return maxNumChannels;
    }

    public void fillSeries(ArrayList<TrackSegment> tracks, Sequence sequence, double diskRadius,
            double outterDiskRadius, int timeClip, final AveragingType averagingMethod)
    {
        // discriminate between new series and existing series to complement

        final ArrayList<TrackAnalysis> analyzedTrkList = new ArrayList<TrackAnalysis>();
        boolean[] frameHasDetection = new boolean[sequence.getSizeT()];
        switch (averagingMethod)
        {
            case DISK:
            {
                ArrayList<TrackAnalysisDisk> analysisList = new ArrayList<TrackAnalysisDisk>();
                for (TrackSegment ts : tracks)
                {
                    TrackGroup group = ts.getOwnerTrackGroup();
                    TrackAnalysisDisk analysis;
                    if (group == null)
                        analysis = new TrackAnalysisDisk(ts, sequence, trackPool.getTrackSegmentList().indexOf(ts) + "",
                                averagingMethod);
                    else
                        analysis = new TrackAnalysisDisk(ts, sequence,
                                group.getTrackSegmentList().indexOf(ts) + "#" + group.getDescription(),
                                averagingMethod);
                    analysisList.add(analysis);
                    analyzedTrkList.add(analysis);

                    for (int t = Math.max(0, ts.getFirstDetection().getT() - timeClip); t <= Math
                            .min(sequence.getSizeT() - 1, ts.getLastDetection().getT() + timeClip); t++)
                        frameHasDetection[t] = true;
                }
                for (int t = 0; t < sequence.getSizeT(); t++)
                {
                    if (frameHasDetection[t])
                    {
                        double[][][] imageTab = new double[sequence.getSizeZ()][sequence.getSizeC()][];
                        for (int z = 0; z < sequence.getSizeZ(); z++)
                            for (int c = 0; c < sequence.getSizeC(); c++)
                                imageTab[z][c] = (double[]) ArrayUtil.arrayToDoubleArray(
                                        sequence.getImage(t, z, c).getDataXY(0), sequence.isSignedDataType());
                        for (TrackAnalysisDisk ta : analysisList)
                            ta.fillAveragingSeriesAtT(t, timeClip, imageTab, diskRadius);
                    }
                }
                break;
            }
            case SPOT_MASK:
            {
                ArrayList<TrackAnalysisMask> analysisList = new ArrayList<TrackAnalysisMask>();
                for (TrackSegment ts : tracks)
                {
                    TrackGroup group = ts.getOwnerTrackGroup();
                    TrackAnalysisMask analysis;
                    if (group == null)
                        analysis = new TrackAnalysisMask(ts, sequence, trackPool.getTrackSegmentList().indexOf(ts) + "",
                                averagingMethod);
                    else
                        analysis = new TrackAnalysisMask(ts, sequence,
                                group.getTrackSegmentList().indexOf(ts) + "#" + group.getDescription(),
                                averagingMethod);
                    analysisList.add(analysis);
                    analyzedTrkList.add(analysis);

                    for (int t = Math.max(0, ts.getFirstDetection().getT() - timeClip); t <= Math
                            .min(sequence.getSizeT() - 1, ts.getLastDetection().getT() + timeClip); t++)
                        frameHasDetection[t] = true;
                }
                for (int t = 0; t < sequence.getSizeT(); t++)
                {
                    if (frameHasDetection[t])
                    {
                        double[][][] imageTab = new double[sequence.getSizeZ()][sequence.getSizeC()][];
                        for (int z = 0; z < sequence.getSizeZ(); z++)
                            for (int c = 0; c < sequence.getSizeC(); c++)
                                imageTab[z][c] = (double[]) ArrayUtil.arrayToDoubleArray(
                                        sequence.getImage(t, z, c).getDataXY(0), sequence.isSignedDataType());
                        for (TrackAnalysisMask ta : analysisList)
                            ta.fillAveragingSeriesAtT(t, timeClip, imageTab, diskRadius);
                    }
                }
                break;
            }

            case DISK_BACKGROUND_CORRECTED:
            {
                ArrayList<TrackAnalysisDiskNoBackground> analysisList = new ArrayList<TrackAnalysisDiskNoBackground>();
                for (TrackSegment ts : tracks)
                {
                    TrackGroup group = ts.getOwnerTrackGroup();
                    TrackAnalysisDiskNoBackground analysis;
                    if (group == null)
                        analysis = new TrackAnalysisDiskNoBackground(ts, sequence,
                                trackPool.getTrackSegmentList().indexOf(ts) + "", averagingMethod);
                    else
                        analysis = new TrackAnalysisDiskNoBackground(ts, sequence,
                                group.getTrackSegmentList().indexOf(ts) + "#" + group.getDescription(),
                                averagingMethod);
                    analysisList.add(analysis);
                    analyzedTrkList.add(analysis);
                    for (int t = Math.max(0, ts.getFirstDetection().getT() - timeClip); t <= Math
                            .min(sequence.getSizeT() - 1, ts.getLastDetection().getT() + timeClip); t++)
                        frameHasDetection[t] = true;
                }
                for (int t = 0; t < sequence.getSizeT(); t++)
                {
                    if (frameHasDetection[t])
                    {
                        double[][][] imageTab = new double[sequence.getSizeZ()][sequence.getSizeC()][];
                        for (int z = 0; z < sequence.getSizeZ(); z++)
                            for (int c = 0; c < sequence.getSizeC(); c++)
                                imageTab[z][c] = (double[]) ArrayUtil.arrayToDoubleArray(
                                        sequence.getImage(t, z, c).getDataXY(0), sequence.isSignedDataType());
                        for (TrackAnalysisDiskNoBackground ta : analysisList)
                            ta.fillAveragingSeriesAtT(t, timeClip, imageTab, diskRadius, outterDiskRadius);
                    }
                }
                break;
            }
            default:
                break;
        }

        analyzedTracks.addAll(analyzedTrkList);
    }

    @Override
    public void Compute()
    {
        // TODO Auto-generated method stub

    }
}

package plugins.lagache.exporttrack;

import icy.sequence.Sequence;
import plugins.fab.trackmanager.TrackSegment;


/** Object to manage intensity values of a TrackSegment through time
 * 
 * @author nicolas chenouard
 * 
 * @date 11/03/2012
 *
 * */

abstract class TrackAnalysis
{
	TrackSegment track;
	Sequence sequence;
	String description;
	AveragingType averagingType;
	
	int halfCropSize = 5;

	TrackAnalysis(TrackSegment track, Sequence sequence, String description, AveragingType averagingMethod)
	{
		if (track == null)
			throw new IllegalArgumentException("NULL TrackSegment object is not allowed for creating a TrackAnalysis instance");
		if (sequence == null)
			throw new IllegalArgumentException("NULL Sequence object is not allowed for creating a TrackAnalysis instance");
		if (description == null)
			throw new IllegalArgumentException("NULL Sequence object is not allowed for creating a TrackAnalysis instance");
		this.track = track;
		this.sequence = sequence;
		this.description = description;
		this.averagingType = averagingMethod;
	};

	abstract public void clearSeries();
}
	
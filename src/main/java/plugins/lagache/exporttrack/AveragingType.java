package plugins.lagache.exporttrack;

enum AveragingType {DISK, DISK_BACKGROUND_CORRECTED, SPOT_MASK}